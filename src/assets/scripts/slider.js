const sliderItems = document.getElementById("sliderItems")
const sliderNextBtn = document.getElementById("slideNextBtn")
const sliderPrevBtn = document.getElementById("slidePrevBtn")
let count = 0;

sliderNextBtn.addEventListener("click", (e) => {
    sliderItems.style.transform = `translateX(${count-=100}%)`
})

sliderPrevBtn.addEventListener("click", () => {
    if (count !== 0) {
        sliderItems.style.transform = `translateX(${(count+=100)}%)`
    }
})